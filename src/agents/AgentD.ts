import { Agent} from "../Agent";
import { MaybeCell, ScreenPart, Motion} from "../types";

export class AgentD implements Agent {
  initialize(): void {
    // No state to initialize
  }

  move(screenPart: ScreenPart): Motion {
    for (let i = 0; i < 5; i++) {
      for (let j = 0; j < 5; j++) {
        if (screenPart[j][i] == "apple") {
          if (i > 3) return "right";
          else if (i < 3) return "left";
          else if (j > 3) return "down";
          else if (j < 3) return "up";
        }
      }
    }
    return this.randomMotion(screenPart);
  }

  private randomMotion(part: ScreenPart): Motion {
    const rnd: number = Math.random() * 4;

    let x: Motion;
    if (rnd < 1) x = "up";
    else if (rnd < 2) x = "down";
    else if (rnd < 3) x = "left";
    else x = "right";

    // try not to hit anything
    if (this.tryMove(x, part) != "apple" && this.tryMove(x, part) != "empty") {
      switch (x) {
        case "up": return "down";
        case "right": return "left";
        case "down": return "up";
        case "left": return "right";
      }
    }

    return x;
  }

  private tryMove(m: Motion, p: ScreenPart): MaybeCell {
    switch (m) {
      case "left": return p[2][1];
      case "right": return p[2][3];
      case "up": return p[1][2];
      case "down": return p[3][2];
    }
  }
}
