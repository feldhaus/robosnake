import { Agent } from "../Agent";
import { ScreenPart, Motion, MaybeCell } from "../types";

//EdgeAgent follows the edge of the map until it dies.
export class EdgeAgent implements Agent {

  curDir: Motion = "up"
  initialize(): void {
  }


  move(screenPart: ScreenPart): Motion {
    const topEdge =  screenPart[0][2] == "outside";
    const bottomEdge = screenPart[4][2] == "outside";
    const leftEdge =  screenPart[2][0] == "outside";
    const rightEdge = screenPart[2][4] == "outside";

    if(topEdge && leftEdge) {
        this.curDir = "right"
    }
    if(topEdge && rightEdge) {
        this.curDir = "down"
    };
    if(bottomEdge && rightEdge) {
        this.curDir = "left"
    }
    if(bottomEdge && leftEdge) {
        this.curDir = "up"
    }
    console.log(screenPart);
    console.log(this.curDir);
    return this.curDir;
  }
}