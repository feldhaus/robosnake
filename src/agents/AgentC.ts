import { Agent} from "../Agent";
import { ScreenPart, Motion} from "../types";

export class AgentC implements Agent {
  private cCycle: Motion[] = ["up", "up", "right", "down", "right"];
  private cIndex: number = 0;

  initialize(): void {
    this.cIndex = 0;
  }

  move(screenPart: ScreenPart): Motion {
    const c: Motion = this.cCycle[this.cIndex];
    this.cIndex = (this.cIndex + 1) % this.cCycle.length;
    return c;
  }
}
