import { Agent} from "../Agent";
import { MaybeCell, ScreenPart, Motion} from "../types";

export class AgentB implements Agent {
  initialize(): void {
    // No state to initialize
  }

  move(screenPart: ScreenPart): Motion {
    return this.randomMotion(screenPart);
  }

  private randomMotion(part: ScreenPart): Motion {
    const rnd: number = Math.random() * 4;

    let x: Motion;
    if (rnd < 1) x = "up";
    else if (rnd < 2) x = "down";
    else if (rnd < 3) x = "left";
    else x = "right";

    // try not to hit anything
    let move = this.tryMove(x, part);
    if (move != "apple" && move != "empty") {
      switch (x) {
        case "up": return "down";
        case "right": return "left";
        case "down": return "up";
        case "left": return "right";
      }
    }

    return x;
  }

  private tryMove(m: Motion, p: ScreenPart): MaybeCell {
    switch (m) {
      case "left": return p[2][1];
      case "right": return p[2][3];
      case "up": return p[1][2];
      case "down": return p[3][2];
    }
  }
}
