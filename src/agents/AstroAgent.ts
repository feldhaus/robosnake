import { Agent } from "../Agent";
import { ScreenPart, Motion, MaybeCell } from "../types";

// AstroAgent decides its movements based on its astrological chart,
// which are randomly generated at "birth".
export class AstroAgent implements Agent {
  private zodiacSign: ZodiacSign = "Aries";
  private elementalSign: ElementalSign = "Air";
  private currentDirection: Motion = "down";

  initialize(): void {
    this.zodiacSign = this.getZodiacSign();
    this.elementalSign = this.getElementalSign();
    this.currentDirection = this.getInitialDirection();
  }

  move(screenPart: ScreenPart): Motion {
    const cellInFront = this.getCellInFront(screenPart);

    if (cellInFront === "apple") {
      return this.currentDirection;
    }

    if (cellInFront === "empty") {
      const shouldChangeDirection = this.shouldChangeDirection();
      if (shouldChangeDirection) {
        this.currentDirection = this.getNewDirection();
      }
      return this.currentDirection;
    }

    this.currentDirection = this.getNewDirection();
    return this.currentDirection;
  }

  private getZodiacSign(): ZodiacSign {
    const signs: ZodiacSign[] = ["Aries", "Taurus", "Gemini", "Cancer", "Leo", "Virgo", "Libra", "Scorpio", "Sagittarius", "Capricorn", "Aquarius", "Pisces"];
    const randomIndex = Math.floor(Math.random() * signs.length);
    return signs[randomIndex];
  }

  private getElementalSign(): ElementalSign {
    const signs: ElementalSign[] = ["Fire", "Earth", "Air", "Water"];
    const randomIndex = Math.floor(Math.random() * signs.length);
    return signs[randomIndex];
  }

  private getInitialDirection(): Motion {
    const directions: Motion[] = ["up", "right", "down", "left"];
    const randomIndex = Math.floor(Math.random() * directions.length);
    return directions[randomIndex];
  }

  private getCellInFront(screenPart: ScreenPart): MaybeCell {
    const row = Math.floor(screenPart.length / 2);
    const col = Math.floor(screenPart[0].length / 2);

    switch (this.currentDirection) {
      case "up":
        return screenPart[1][2];
      case "right":
        return screenPart[2][3];
      case "down":
        return screenPart[3][2];
      case "left":
        return screenPart[2][1];
    }
  }

  private shouldChangeDirection(): boolean {
    const changeDirectionProbability = this.getChangeDirectionProbability();
    return Math.random() < changeDirectionProbability;
  }

  private getChangeDirectionProbability(): number {
    switch (this.zodiacSign) {
      case "Aries":
        return 1;
      case "Leo":
        return 0.8;
      case "Sagittarius":
        return 0.3;
      case "Taurus":
        return 0.9;
      case "Virgo":
        return 0.4;
      case "Capricorn":
        return 0.1;
      case "Gemini":
        return 0.19
      case "Libra":
        return 0.6;
      case "Aquarius":
        return 0.5;
      case "Cancer":
        return 0.1;
      case "Scorpio":
        return 1;
      case "Pisces":
        return 0.2;
    }
  }

  private getNewDirection(): Motion {
    const directions: Motion[] = ["up", "right", "down", "left"];
    const currentIndex = directions.indexOf(this.currentDirection);
    let newIndex = currentIndex;

    switch (this.elementalSign) {
      case "Fire":
        newIndex = (currentIndex + 1) % directions.length;
        break;
      case "Earth":
        newIndex = (currentIndex + 2) % directions.length;
        break;
      case "Air":
        newIndex = (currentIndex + 3) % directions.length;
        break;
      case "Water":
        newIndex = (currentIndex + Math.floor(Math.random() * 4)) % directions.length;
        break;
    }

    return directions[newIndex];
  }
}

type ZodiacSign = "Aries" | "Taurus" | "Gemini" | "Cancer" | "Leo" | "Virgo" | "Libra" | "Scorpio" | "Sagittarius" | "Capricorn" | "Aquarius" | "Pisces";
type ElementalSign = "Fire" | "Earth" | "Air" | "Water";