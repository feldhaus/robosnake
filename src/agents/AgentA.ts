import { Agent} from "../Agent";
import { ScreenPart, Motion} from "../types";

export class AgentA implements Agent {
  initialize(): void {

  }

  move(screenPart: ScreenPart): Motion {
    return "right";
  }
}