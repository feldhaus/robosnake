import { resetCanvas } from "./DrawingLibrary";
import { GameRunner } from "./GameRunner";
import { initialize } from "./GameScreen";
import { AgentA } from "./agents/AgentA";
import { AgentB } from "./agents/AgentB";
import { AgentC } from "./agents/AgentC";
import { AgentD } from "./agents/AgentD";
import { EdgeAgent } from "./agents/EdgeAgent";
import { AstroAgent } from "./agents/AstroAgent";

export function play(): void {
  resetCanvas();

  const screen = initialize(50);
  // Add new agents here - more than zero, less than five.
  const agents = [
    new AgentA(),
    new AgentB(),
    new AgentC(),
    new AgentD()
  ];

  const gameRunner = new GameRunner(agents);
  gameRunner.run(1000, 10, screen);
}