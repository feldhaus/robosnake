export type Motion = "up" | "down" | "left" | "right";
export type MaybeCell = Cell | "outside";
export type ScreenPart = MaybeCell[][];
export type Player = "A" | "B" | "C" | "D";
export type Cell = "empty" | "apple" | Player;


export class Point {
  public x: number;
  public y: number;

  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }
}

export class SnakeState extends Point {
  public apples: number;
  public lost: boolean;

  constructor(x: number, y: number) {
    super(x, y); // call Point constructor to set x and y
    this.apples = 0;
    this.lost = false;
  }

  public setPoint(p: Point): void {
    this.x = p.x;
    this.y = p.y;
  }
}
