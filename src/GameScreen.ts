import { Player } from "./types";
import { fillCell } from "./DrawingLibrary";

// a Cell is either a Player or the string "empty" or "apple"
export type Cell = Player | "empty" | "apple";

// a GameScreen is an array of Cell arrays
export type GameScreen = Cell[][]; // row-major order, should always have square dimensions

const colorMap = {
  "empty": "white",
  "apple": "red",
  "A": "green",
  "B": "blue",
  "C": "orange",
  "D": "purple"
}

export function initialize(size: number): GameScreen {
  const screen: GameScreen = [];
  for (let y = 0; y < size; y++) {
    const row: Cell[] = [];
    for (let x = 0; x < size; x++) {
      row.push("empty");
    }
    screen.push(row);
  }
  return screen;
}

export function draw(screen: GameScreen): void {
  for (let y = 0; y < screen.length; y++) {
    for (let x = 0; x < screen.length; x++) {
      const cell = screen[y][x];
      const color = colorMap[cell];
      fillCell(color, x, y);
    }
  }
}