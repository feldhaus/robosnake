import { ScreenPart, Motion} from "./types";

export interface Agent {
  initialize(): void;
  move(screenPart: ScreenPart): Motion;
}