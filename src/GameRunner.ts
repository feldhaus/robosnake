import { scheduleNextUpdate, updateApples, updateLost } from "./DrawingLibrary";
import { draw, GameScreen } from "./GameScreen";
import { Agent } from "./Agent";
import { Motion, MaybeCell, ScreenPart, SnakeState, Point, Player } from "./types";

class AgentState {
  constructor(public agent: Agent, public state: SnakeState) {}
}

export class GameRunner {
  private agentStates: Partial<Record<Player, AgentState>> = {};

  constructor(agents: Agent[]) {
    if (agents.length == 0) {
      throw new Error("Cannot run game with zero agents.");
    }
    if (agents.length > 4) {
      throw new Error("Cannot run game with more than four agents.");
    }
    const players: Player[] = ["A", "B", "C", "D"];
    for (let i = 0; i < agents.length; i++) {
      this.agentStates[players[i]] = new AgentState(agents[i], new SnakeState(0, 0));
    }
  }


  public run(stepTime: number, newApplesEachStep: number, screen: GameScreen): void {
    this.initializeStates(screen.length, screen.length);
    this.initializeAgents();
    this.drawStates(screen);
    draw(screen);

    // schedule first step
    scheduleNextUpdate(stepTime, () => this.step(stepTime, newApplesEachStep, screen));
  }

  private initializeStates(screenWidth: number, screenHeight: number): void {
    const playerStates: Record<Player, SnakeState> = {
      A: new SnakeState(0, 0),
      B: new SnakeState(screenHeight - 1, 0),
      C: new SnakeState(0, screenWidth - 1),
      D: new SnakeState(screenHeight - 1, screenWidth - 1)
    };
    Object.keys(this.agentStates).forEach(key => {
      const player = key as Player;
      const agentState = this.agentStates[player];
      if (agentState) {
        agentState.state = playerStates[player];
      }
    });
  }

  private step(stepTime: number, newApplesEachStep: number, screen: GameScreen): void {
    this.addApples(newApplesEachStep, screen);
    this.updateSnakes(screen);
    draw(screen);

    this.updateLost();
    this.updateApples();
    if (!this.allSnakesLost()) {
      scheduleNextUpdate(stepTime, () => this.step(stepTime, newApplesEachStep, screen));
    }
  }

  private initializeAgents(): void {
    Object.keys(this.agentStates).forEach(key => {
      const player = key as Player;
      const agentState = this.agentStates[player];
      if (agentState) {
        agentState.agent.initialize();
      }
    });
  }

  private drawStates(screen: GameScreen): void {
    Object.keys(this.agentStates).forEach(key => {
      const player = key as Player;
      const agentState = this.agentStates[player];
      if (agentState) {
        const state = agentState.state;
        screen[state.y][state.x] = player;
      }
    });
  }

  private addApples(newApplesEachStep: number, screen: GameScreen): void {
    for (let i = 0; i < newApplesEachStep; i++) {
      const x = Math.floor(Math.random() * screen.length);
      const y = Math.floor(Math.random() * screen.length);
      if (screen[y][x] == "empty") screen[y][x] = "apple";
    }
  }

  private updateSnakes(screen: GameScreen): void {
    Object.keys(this.agentStates).forEach(key => {
      const player = key as Player;
      const agentState = this.agentStates[player];
      if (agentState) {
        this.updateSnake(screen, agentState.agent, agentState.state, player);
      }
    });
  }

  private updateApples(): void {
    Object.keys(this.agentStates).forEach(key => {
      const player = key as Player;
      const agentState = this.agentStates[player];
      if (agentState) {
        updateApples(player, agentState.state.apples)
      }
    });
  }

  private updateSnake(screen: GameScreen, agent: Agent, state: SnakeState, player: Player): void {
    if (state.lost) {
      return;
    }
    const screenPart = this.getScreenPart(screen, state);
    const move = agent.move(screenPart);
    const newPosition = this.locationAfterMotion(move, state);
    if (newPosition.x < 0 || newPosition.y < 0 || newPosition.x >= screen.length || newPosition.y >= screen.length) {
      state.lost = true;
      return;
    }
    const cellContents = screen[newPosition.y][newPosition.x];
    switch (cellContents) {
      case "empty":
        state.setPoint(newPosition);
        screen[newPosition.y][newPosition.x] = player;
        break;
      case "apple":
        state.setPoint(newPosition);
        state.apples++;
        screen[newPosition.y][newPosition.x] = player;
        break;
      default:
        state.lost = true;
        break;
    }
  }

  private updateLost(): void {
    Object.keys(this.agentStates).forEach(key => {
      const player = key as Player;
      const agentState = this.agentStates[player];
      if (agentState) {
        updateLost(player, agentState.state.lost);
      }
    });
  }

  private allSnakesLost(): boolean {
    return Object.keys(this.agentStates).every(key => {
      const agentState = this.agentStates[key as Player];
      return agentState ? agentState.state.lost : true;
    });
  }


  private locationAfterMotion(motion: Motion, snake: SnakeState): Point {
    return new Point(
      snake.x + (motion == "left" ? -1 : motion == "right" ? 1 : 0),
      snake.y + (motion == "up" ? -1 : motion == "down" ? 1 : 0)
    )
  }

  private getScreenPart(screen: GameScreen, s: SnakeState): ScreenPart {
    const part: ScreenPart = new Array<MaybeCell[]>(5);
    for (let j = 0; j < 5; j++) {
      part[j] = new Array<MaybeCell>(5);
      for (let i = 0; i < 5; i++) {
        const in_bounds_x = s.x + i - 2 >= 0 && s.x - 2 + i < screen.length;
        const in_bounds_y =  s.y - 2 + j >= 0 && s.y - 2 + j < screen.length;
        if (in_bounds_x && in_bounds_y) {
          part[j][i] = screen[s.y + j - 2][s.x + i - 2];
        } else {
          part[j][i] = "outside";
        }
      }
    }
    return part;
  }
}
